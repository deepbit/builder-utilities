package main

import (
	"net/http"

	"github.com/takama/daemon"
)

func main() {
	println("Stating healthz server on port 8000")
	http.HandleFunc("/healthz", healthzHandler)
	println("Listening...")

	http.ListenAndServe(":8000", nil)
}

func healthzHandler(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("OK!"))
}
